<?php

class ContactController extends BaseController {


    /**
     * Called via ajax to add mail to mailing list.
     * @param $email
     * @return mixed
     */
    public function sendMessage() {

        $data = Input::all();
        Mail::send('emails.contact-form', array('data' => $data), function($message)
        {
            $message->to('kontakt@vrticuniverzum.com')->subject('Poruka za vrtic Univerzum');
        });
    }

}
