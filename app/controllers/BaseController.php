<?php

class BaseController extends Controller {

    public function __construct() {
        $base_url = URL::to('/');
        View::share('js_base_url', '<script> base_url = "'. $base_url .'";</script>');
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
