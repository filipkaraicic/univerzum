<?php

class LandingController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Landing page
    |--------------------------------------------------------------------------
    */
    public function index()
    {
        $ponuda = Tekstovi::getText('ponuda');
        View::share('ponuda', $ponuda->text);

        $program = Tekstovi::getText('program');
        View::share('program', $program->text);

        $dan = Tekstovi::getText('dan');
        View::share('dan', $dan->text);

        $prostor = Tekstovi::getText('prostor');
        View::share('prostor', $prostor->text);

        return View::make('pages.home');
    }

    /**
     * Called via ajax to add mail to mailing list.
     * @param $email
     * @return mixed
     */
    public function addMail() {

        $email = Input::get('email');
        if(MailingList::addEmail($email))
            return Response::json('success');
        else
            return Response::json('false');
    }

}
