<?php

class AuthController extends BaseController {

    public function index(){
        return \View::make('pages.login');
    }

    public function login() {
        $params = array();
        $params['username'] = Input::get('username');
        $params['password'] = Input::get('password');

        if (\Auth::attempt($params))
        {
            $user = User::where('username', $params['username'])->first();
            Session::put('userId', $user->id);
            Session::put('username', $user->username);
            return Redirect::route('podesavanja');
        }
        else
            return \Redirect::route('login')
                ->with('error', 'Pogresan username ili sifra.')
                ->withInput();

    }

    public function logout() {
        $username = Session::get('username');
        Auth::logout();
        Session::flush();
        return Redirect::route('login');
    }

}
