<?php

namespace Controllers\Dashboard;

class SettingsController extends \BaseController {


    public function index()
    {
        return \View::make('sites.dashboard.home');
    }

    public function mojsajt() {
        return \Helpers::redirectTo(\Session::get('username'));
    }

}
