<?php

namespace Controllers\Dashboard;

class DanController extends \BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function index()
    {
        $dan = \Tekstovi::getText('dan');
        if (!empty($dan)) {
            $breaks = array("<br />","<br>","<br/>");
            $text = str_ireplace($breaks, "", $dan->text);
        } else {
            $text = "";
        }
        return \View::make('sites.dashboard.dan', compact('dan', 'text'));
    }

    public function update() {
        $text = \Input::get('text');
        \Tekstovi::setText($text, 'dan');
        return \Redirect::back()->with('success', 'Uspesno ste postavili tekst.');
    }

}
