<?php

namespace Controllers\Dashboard;

class ProstorController extends \BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function index()
    {
        $prostor = \Tekstovi::getText('prostor');
        if (!empty($prostor)) {
            $breaks = array("<br />","<br>","<br/>");
            $text = str_ireplace($breaks, "", $prostor->text);
        } else {
            $text = "";
        }
        return \View::make('sites.dashboard.prostor', compact('prostor', 'text'));
    }

    public function update() {
        $text = \Input::get('text');
        \Tekstovi::setText($text, 'prostor');
        return \Redirect::back()->with('success', 'Uspesno ste postavili tekst.');
    }

}
