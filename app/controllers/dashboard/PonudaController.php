<?php

namespace Controllers\Dashboard;

class PonudaController extends \BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function index()
    {
        $ponuda = \Tekstovi::getPonuda();
        if (!empty($ponuda)) {
            $breaks = array("<br />","<br>","<br/>");
            $text = str_ireplace($breaks, "", $ponuda->text);
        } else {
            $text = "";
        }
        return \View::make('sites.dashboard.ponuda', compact('ponuda', 'text'));
    }

    public function update() {
        $text = \Input::get('text');
        \Tekstovi::setPonuda($text);
        return \Redirect::back()->with('success', 'Uspesno ste postavili tekst.');
    }

}
