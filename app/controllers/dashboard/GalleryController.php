<?php

namespace Controllers\Dashboard;

use Illuminate\Support\Facades\View;

class GalleryController extends \BaseController {

    const SCALE = 3;

    public function index() {
        return View::make('public.sites.dashboard.newphoto');
    }

    public function viewPhotos(){
        $photos = \Media::where('user_id', '=', \Session::get('userId'))->where('type', '=', 'gallery')->get();
        return View::make('public.sites.dashboard.viewphotos', compact('photos'));
    }

    public static function savePhoto(){
        $imagePath =public_path('assets/gallery/' .  \Session::get('userId') . '/');

        if (!file_exists($imagePath)) {
            mkdir($imagePath, 0777, true);
        }

        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);

        //Check write Access to Directory

        if(!is_writable($imagePath)){
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t upload File; no write Access'
            );
            print json_encode($response);
            return;
        }

        if ( in_array($extension, $allowedExts))
        {
            if ($_FILES["img"]["error"] > 0)
            {
                $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
                );
            }
            else
            {

                $filename = $_FILES["img"]["tmp_name"];
                list($width, $height) = getimagesize( $filename );

                move_uploaded_file($filename,  $imagePath . $_FILES["img"]["name"]);

                $response = array(
                    "status" => 'success',
                    "url" => $imagePath.$_FILES["img"]["name"], // asset('assets/photos/' . $_FILES["img"]["name"]),
                    "imageUrl" => asset('assets/gallery/'. \Session::get('userId') . '/' . $_FILES["img"]["name"]),
                    "width" => $width,
                    "height" => $height
                );

            }
        }
        else
        {
            $response = array(
                "status" => 'error',
                "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
            );
        }

        print json_encode($response);
    }


    public static function cropPhoto() {

        /*
*	!!! THIS IS JUST AN EXAMPLE !!!, PLEASE USE ImageMagick or some other quality image processing libraries
*/

        $imgUrl = $_POST['imgUrl'];
        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW = $_POST['imgW'] * Self::SCALE;
        $imgH = $_POST['imgH']* Self::SCALE;
        // offsets
        $imgY1 = $_POST['imgY1']* Self::SCALE;
        $imgX1 = $_POST['imgX1']* Self::SCALE;
        // crop box
        $cropW = $_POST['cropW']* Self::SCALE;
        $cropH = $_POST['cropH']* Self::SCALE;
        // rotation angle
        $angle = $_POST['rotation'];

        $jpeg_quality = 100;

        //$output_filename = "temp/croppedImg_".rand();

        // uncomment line below to save the cropped image in the same location as the original image.
        $file_name_ext = basename($imgUrl);
        $path_parts = pathinfo($imgUrl);
        $file_name = $path_parts['filename'];
        $cropped_name = "cropped_" . $file_name;
        $cropped_name_ext = "cropped_" . $file_name_ext;
        $output_filename = dirname($imgUrl). "/".$cropped_name;

        //$output_filename = asset('assets/cropped_photos/' + rand() );

        $what = getimagesize($imgUrl);

        switch(strtolower($what['mime']))
        {
            case 'image/png':
                $img_r = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type = '.png';
                break;
            case 'image/jpeg':
                $img_r = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                error_log("jpg");
                $type = '.jpeg';
                break;
            case 'image/gif':
                $img_r = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type = '.gif';
                break;
            default: die('image type not supported');
        }


        //Check write Access to Directory

        if(!is_writable(dirname($output_filename))){
            $response = Array(
                "status" => 'error',
                "message" => 'Can`t write cropped File'
            );
        }else{

            // resize the original image to size of editor
            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
            // rotate the rezized image
            $rotated_image = imagerotate($resizedImage, -$angle, 0);
            // find new width & height of rotated image
            $rotated_width = imagesx($rotated_image);
            $rotated_height = imagesy($rotated_image);
            // diff between rotated & original sizes
            $dx = $rotated_width - $imgW;
            $dy = $rotated_height - $imgH;
            // crop rotated image to fit into original rezized rectangle
            $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
            imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
            imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
            // crop image into selected area
            $final_image = imagecreatetruecolor($cropW, $cropH);
            imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
            imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
            // finally output png image
//            imagepng($final_image, $output_filename.$type, $png_quality);
            imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
            $response = Array(
                "status" => 'success',
                "url2" => $output_filename.$type,
                "url" => asset('assets/gallery/'. \Session::get('userId') . '/' . $cropped_name . $type)
            );
        }
        print json_encode($response);

    }


    public static function addPhoto() {
        \Media::addGalleryPhoto();
        return \Redirect::back()->with('success', 'Slika uspesno dodata.');
    }
}