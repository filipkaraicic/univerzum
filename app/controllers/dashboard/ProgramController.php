<?php

namespace Controllers\Dashboard;

class ProgramController extends \BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function index()
    {
        $program = \Tekstovi::getText('program');
        if (!empty($program)) {
            $breaks = array("<br />","<br>","<br/>");
            $text = str_ireplace($breaks, "", $program->text);
        } else {
            $text = "";
        }
        return \View::make('sites.dashboard.program', compact('program', 'text'));
    }

    public function update() {
        $text = \Input::get('text');
        \Tekstovi::setText($text, 'program');
        return \Redirect::back()->with('success', 'Uspesno ste postavili tekst.');
    }

}
