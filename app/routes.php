<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::get('/', function()
//{
//    return View::make('hello');
//});


# Home
Route::get('/', array('as' => 'landing', 'uses' => 'LandingController@index'));

# Mailing List
Route::post('/addMail', array('as' => 'add-mail', 'uses' => 'LandingController@addMail'));

# Mailing List
Route::post('/send-message', array('as' => 'send-message', 'uses' => 'ContactController@sendMessage'));

Route::get('login', array('as' => 'login', 'uses' => 'AuthController@index'));
Route::post('login', array('as' => 'login', 'uses' => 'AuthController@login'));

Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@logout'));

/**
 * Member Routes
 */
Route::group(['prefix' => 'podesavanja', 'before' => 'auth', 'namespace' => 'Controllers\Dashboard'], function () {

    Route::get('/', array('as' => 'podesavanja', 'uses' => 'SettingsController@index'));
    Route::get('/mojsajt', array('as' => 'mojsajt', 'uses' => 'SettingsController@mojsajt'));

    Route::get('/ponuda', array('as' => 'ponuda', 'uses' => 'PonudaController@index'));
    Route::post('/ponuda', array('as' => 'ponuda', 'uses' => 'PonudaController@update'));

    Route::get('/program', array('as' => 'program', 'uses' => 'ProgramController@index'));
    Route::post('/program', array('as' => 'program', 'uses' => 'ProgramController@update'));

    Route::get('/dan', array('as' => 'dan', 'uses' => 'DanController@index'));
    Route::post('/dan', array('as' => 'dan', 'uses' => 'DanController@update'));

    Route::get('/prostor', array('as' => 'prostor', 'uses' => 'ProstorController@index'));
    Route::post('/prostor', array('as' => 'prostor', 'uses' => 'ProstorController@update'));

    Route::get('/novaslika', array('as' => 'novaslika', 'uses' => 'GalleryController@index'));
    Route::post('/novaslika', array('as' => 'novaslika', 'uses' => 'GalleryController@addPhoto'));

    Route::get('/pregledslika', array('as' => 'pregledslika', 'uses' => 'GalleryController@viewphotos'));

    # Upload photos
    Route::post('upload-photo', array('as' => 'podesavanja-upload-photo', 'uses' => 'GalleryController@savePhoto'));

    # Crop photos
    Route::post('crop-photo', array('as' => 'podesavanja-crop-photo', 'uses' => 'GalleryController@cropPhoto'));

});
