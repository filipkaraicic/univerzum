<?php


class MailingList extends Eloquent  {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mailing_list';


    public static function addEmail($email) {

        $rules = [
            'email' => 'required|email|unique:mailing_list',
        ];

        $input = Input::only(
            'email'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return false;
        }

        $mailing_list = new MailingList();
        $mailing_list->email = $email;
        $mailing_list->save();
        return true;
    }

}
