<?php

use Illuminate\Support\Facades\DB;

class Tekstovi extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tekstovi';

    public static function getPonuda(){
        $text = Tekstovi::where('naziv', "=", 'ponuda')->get();
        if (!$text->isEmpty())
            $text = $text->first();
        else
            $text = "";
        return $text;
    }

    public static function setPonuda($text){
        $hasText = Tekstovi::where('naziv', "=", 'ponuda')->get();
        if (!$hasText->isEmpty()) {
            $ponuda = Tekstovi::where('naziv', '=', 'ponuda')->first();
            $ponuda->naziv = 'ponuda';
            $ponuda->text = $text;
            $ponuda->save();
        } else {
            $ponuda = new Tekstovi();
            $ponuda->naziv = 'ponuda';
            $ponuda->text = $text;
            $ponuda->save();
        }
    }

    public static function getText($naziv){
        $text = Tekstovi::where('naziv', "=", $naziv)->get();
        if (!$text->isEmpty())
            $text = $text->first();
        else {
            $text = new StdClass();
            $text->text = "";

        }
        return $text;
    }

    public static function setText($text, $naziv){
        $hasText = Tekstovi::where('naziv', "=", $naziv)->get();
        if (!$hasText->isEmpty()) {
            $ponuda = Tekstovi::where('naziv', '=', $naziv)->first();
            $ponuda->text = $text;
            $ponuda->naziv = $naziv;
            $ponuda->save();
        } else {
            $ponuda = new Tekstovi();
            $ponuda->naziv = $naziv;
            $ponuda->text = $text;
            $ponuda->save();
        }
    }
}