<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body style="color:black">
<h2>Vrtic Univerzum</h2>
<h3>Ovu poruku vam je poslao {{ $data['name'] }} sa sajta vrtica Univerzum.<br>
    Ukoliko zelite da mu odgovrite posaljite email na: {{ $data['email'] }}</h3>

<div style="padding:10px; background-color: rgba(222, 222, 222, 0.78); border-radius: 5px; color:black;">
<b>Ime:</b> {{ $data['name'] }} <br>
<b>Telefon:</b> {{ $data['phone'] }} <br>
<b>Email:</b> {{ $data['email'] }} <br>
<b>Poruka:</b> <br>
    {{ $data['message'] }}
</div>
</body>
</html>