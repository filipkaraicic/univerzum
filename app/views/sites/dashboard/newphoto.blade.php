@extends('public.sites.dashboard.layouts.dashboard')
@section('page_heading','Nova slika')
@section('section')

    <div class="col-lg-8">
        @section ('pane1_panel_title', 'Dodaj sliku')
        @section ('pane1_panel_body')

        <div class="row">
            <div class="col-lg-10">
                <form method="post" action="{{ route('novaslika') }}">
                    <div class="form-group" style="margin-top:20px;">
                        <div class="col-lg-12" style="text-align: center;">
                            <div id="gallery_photo" class="gallery_photo" style="margin:0 auto;"></div>
                            <br>
                            <div id="upload_photo" class="btn btn-primary" style="margin:0 auto;">Izaberi sliku</div>
                            <input type="hidden" id="photo_url" name="photo_url">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Naslov slike:</label>
                        <input name="title" class="form-control" placeholder="Ovaj naslov ce stajati ispod slike na vasem sajtu">
                    </div>
                    <div class="form-group">
                        <label>Opis slike:</label>
                        <textarea name="description" class="form-control" rows="3" placeholder="Ovde unesite dodatni opis slike koji ce biti prikazan ispod naslova."></textarea>
                    </div>
                    <div class="form-group">
                        <label>Status:</label>
                        <div class="checkbox">
                            <label>
                                <input name="active" type="checkbox" value="1" checked>Prikazi odmah sliku na sajtu (odcekirajte ovo polje ukoliko ne zelite da odmah prikazete sliku vec samo da je dodate u bazu).
                            </label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-default">Sacuvaj i Izmeni</button>
                </form>
            </div>
        </div>



        @endsection
        @include('public.sites.dashboard.widgets.panel', array('header'=>true, 'as'=>'pane1'))

    </div>

    <script>
        $(function(){
            //UPLOAD PHOTOS
            var cropperOptions1 = {
                uploadUrl: base_url + '/podesavanja/upload-photo',
                cropUrl: base_url + '/podesavanja/crop-photo',
                customUploadButtonId: 'upload_photo',
                outputUrlId: 'photo_url'
            }

            var cropper1 = new Croppic('gallery_photo', cropperOptions1);
        })
    </script>

    <style>

        .gallery_photo {
            width: 250px;
            height: 141px;
            position:relative; /* or fixed or absolute */
        }

        .gallery_photo img {
            width:250px;
        }
    </style>
@stop