@extends('public.sites.dashboard.layouts.dashboard')
@section('page_heading','Pregled slika')
@section('section')

    <div class="col-lg-8">
        @section ('pane1_panel_title', 'Pregled i izmena slika.')
        @section ('pane1_panel_body')

            <div class="row">
                <div class="col-lg-10">
                    @foreach($photos as $index=>$photo)
                        <div class="col-md-4 col-sm-6 ">
                            <div class="actions" style="background-color:rgba(0, 0, 0, 0.15);"><i class="fa fa-times fa-2x pull-right"></i> <i class="fa fa-2x fa-pencil-square-o pull-right"></i><div class="clearfix"></div></div>
                               <img src="{{ asset('assets/gallery/' . Session::get('userId') . '/' . $photo->name) }}" class="img-responsive" alt="">

                            <div class="portfolio-caption">
                                <h4>{{ $photo->title }}</h4>
                                <p class="text-muted">{{ $photo->description }}</p>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>



        @endsection
        @include('public.sites.dashboard.widgets.panel', array('header'=>true, 'as'=>'pane1'))

    </div>
@stop