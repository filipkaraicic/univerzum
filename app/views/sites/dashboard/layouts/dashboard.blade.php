@extends('sites.dashboard.layouts.plane')

@section('body')
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('landing') }}">{{ ucfirst(Session::get('username')) }}</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ route('landing') }}"><i class="fa fa-user fa-fw"></i> Pogledaj sajt</a>
                        </li>
                        {{--<li><a href="#"><i class="fa fa-gear fa-fw"></i> Podesavanja</a>--}}
                        {{--</li>--}}
                        <li class="divider"></li>
                        <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Izloguj se</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        {{--<li class="sidebar-search">--}}
                            {{--<div class="input-group custom-search-form">--}}
                                {{--<input type="text" class="form-control" placeholder="Search...">--}}
                                {{--<span class="input-group-btn">--}}
                                {{--<button class="btn btn-default" type="button">--}}
                                    {{--<i class="fa fa-search"></i>--}}
                                {{--</button>--}}
                            {{--</span>--}}
                            {{--</div>--}}
                            {{--<!-- /input-group -->--}}
                        {{--</li>--}}
                        <li {{ (Request::is('podesavanja') ? 'class="active"' : '') }}>
                            <a href="{{ route('podesavanja') }}"><i class="fa fa-dashboard fa-fw"></i> Kontrolna tabla</a>
                        </li>
                        <li {{ (Request::is('*ponuda') ? 'class="active"' : '') }}>
                            <a href="{{ route('ponuda') }}"><i class="fa fa-pencil-square-o fa-fw"></i> Ponuda</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('*program') ? 'class="active"' : '') }}>
                            <a href="{{ route('program') }}"><i class="fa fa-pencil-square-o fa-fw"></i> Program</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('*dan') ? 'class="active"' : '') }}>
                            <a href="{{ route('dan') }}"><i class="fa fa-pencil-square-o fa-fw"></i> Dan u Vrticu</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li {{ (Request::is('*prostor') ? 'class="active"' : '') }}>
                            <a href="{{ route('prostor') }}"><i class="fa fa-pencil-square-o fa-fw"></i> Prostor</a>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
             </div>
            <div class="row">
                <div class="col-lg-11">
                    {{-- Notifications --}}
                    @include('layouts.notifications')
                </div>
            </div>

			<div class="row">  
				@yield('section')

            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@stop

