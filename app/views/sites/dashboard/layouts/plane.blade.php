<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8"/>
	<title>{{ ucfirst(Session::get('username')) }} - PIM</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>

	<link rel="stylesheet" href="{{ asset("assets/sites/dashboard/stylesheets/styles.css") }}" />
    <link rel="stylesheet" href="{{ asset('assets/js/croppic/croppic.css') }}">
    <script src="{{ asset('assets/js/jquery-2.1.3.min.js') }}"></script>
    <script src="{{ asset('assets/js/croppic/croppic.js') }}"></script>
    <script src="{{ asset('assets/js/ckeditor/ckeditor.js') }}"></script>
    {{ $js_base_url }}

</head>
<body>
	@yield('body')
	
	<script src="{{ asset("assets/sites/dashboard/scripts/frontend.js") }}" type="text/javascript"></script>
</body>
</html>