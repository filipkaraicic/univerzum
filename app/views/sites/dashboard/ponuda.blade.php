@extends('sites.dashboard.layouts.dashboard')
@section('page_heading','Ponuda')
@section('section')


    <div class="col-lg-8">
        @section ('pane1_panel_title', 'Ponuda')
        @section ('pane1_panel_body')

        <form method="post" action="{{ route('ponuda') }}">
            <div class="form-group">
                <label>Tekst:</label>
                <textarea id="text" class="form-control" name="text" rows="6" cols="80">{{ $text }}</textarea>
                <p class="help-block">Ovde mozete uneti ili izmeniti tekst koji ce stajati u sekciji ponuda. Nakon sto undesete zeljeni tekst klikom na dugme ispod cete automatski azurirati vas sajt i sacuvati izmene.</p>
                <p class="help-block">@if (!empty($ponuda)) Poslednja izmena: {{ date_format($ponuda->updated_at, "d/m/Y, H:i:s") }} @endif</p>
            </div>
            <button type="submit" class="btn btn-default">Sacuvaj i Izmeni</button>
        </form>


        @endsection
        @include('sites.dashboard.widgets.panel', array('header'=>true, 'as'=>'pane1'))

    </div>


    <script>
        $(function(){
            CKEDITOR.replace( 'text' );
        })
    </script>



@stop