@extends('layouts.default')

@section('content')

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">
                    <div>Univerzum</div>
                    {{--<img src="{{ asset('assets/photos/logo.jpg') }}" width="25px" height="auto">--}}
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">O Nama</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Galerija</a>
                    </li>
                    {{--<li>--}}
                        {{--<a class="page-scroll" href="#mailing-list">Prijavi se!</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<a class="page-scroll" href="#about">Novosti</a>--}}
                    {{--</li>--}}
                    <li>
                        <a class="page-scroll" href="#team">Ljudi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Kontakt</a>
                    </li>
                    {{--<li>--}}
                        {{--@if (Session::has('userId'))--}}
                            {{--<a class="page-scroll" href=""> | Podesavanja</a>--}}
                        {{--@else--}}
                            {{--<a class="page-scroll" href="{{ route('login') }}"> |Uloguj se</a>--}}
                        {{--@endif--}}
                    {{--</li>--}}
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header >
        <div class="intro-text">
            <a href="#services" class="page-scroll btn btn-xl more-button">Saznaj Više</a>
        </div>
        <!-- Insert to your webpage where you want to display the slider -->
        <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:100%;margin:0 auto;">
            <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
                <ul class="amazingslider-slides" style="display:none;">
                    <li><img src="{{ asset('assets/photos/big logo.jpg') }}" alt="001_3" />
                    </li>
                    <li><img src="{{ asset('assets/photos/slike/013.jpg') }}" alt="001_3" />
                    </li>
                    <li><img src="{{ asset('assets/photos/slike/007.jpg') }}" alt="003 Garderoba" />
                    </li>
                    <li><img src="{{ asset('assets/photos/slike/010.jpg') }}" alt="001 Nauka" />
                    </li>
                    <li><img src="{{ asset('assets/photos/slike/016.jpg') }}" alt="010" />
                    </li>
                    {{--<li><img src="{{ asset('assets/slider_images/003%20Predskolsko.jpg') }}" alt="003 Predskolsko" />--}}
                    {{--</li>--}}
                    {{--<li><img src="{{ asset('assets/slider_images/002_4.jpg') }}" alt="002_4" />--}}
                    {{--</li>--}}
                    {{--<li><img src="{{ asset('assets/slider_images/003_3.jpg') }}" alt="003_3" />--}}
                    {{--</li>--}}
                </ul>
                <ul class="amazingslider-thumbnails" style="display:none;">
                    <li><img src="{{ asset('assets/slider_images/001_3-tn.jpg') }}" alt="001_3" /></li>
                    <li><img src="{{ asset('assets/slider_images/003%20Garderoba-tn.jpg') }}" /></li>
                    <li><img src="{{ asset('assets/slider_images/001%20Nauka-tn.jpg') }}" alt="001 Nauka" /></li>
                    <li><img src="{{ asset('assets/slider_images/010-tn.jpg') }}" alt="010" /></li>
                    <li><img src="{{ asset('assets/slider_images/003%20Predskolsko-tn.jpg') }}" alt="003 Predskolsko" /></li>
                    <li><img src="{{ asset('assets/slider_images/002_4-tn.jpg') }}" alt="002_4" /></li>
                    <li><img src="{{ asset('assets/slider_images/003_3-tn.jpg') }}" alt="003_3" /></li>
                </ul>
            </div>
        </div>
        <!-- End of body section HTML codes -->


        {{--<div class="container">--}}




            {{----}}
        {{--</div>--}}
    </header>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">O Nama</h2>
                    <div>
                        <input type="checkbox" class="read-more-state" id="post-1" />

                        <p class="read-more-wrap">
                            Univerzum je sigurno i dobro mesto na kome se poštuje sloboda govora i izbora, otvorenost i jednakost. Vrtić koji raste zajedno sa decom, prati njihov razvoj i potrebe, inspiriše, podstiče na istraživanje i komunikaciju. Želimo da prenesemo i podelimo ono što znamo, da pomognemo malim ljudima da ostanu srećni.
                            Verujemo u bezgraničnost dečijih mogućnosti za pravilan i pozitivan rast i razvoj,
                            Znamo da svako dete ima sposobnost da se oseti srećno i uspešno.
                            <span class="read-more-target">
                                <br>
                                Naš zadatak je da svakom detetu omogućimo uslove da prepoznaje i veruje u sopstvene kapacitete, da stvorimo sredinu u kojoj je voljeno i uči da voli, u kojoj je poštovano i uči da poštuje sebe i druge.
                            Svakom detetu pristupamo sa posebnom pažnjom i uverenjem da je svako dete ličnost za sebe sa potpuno različitim potrebama, sposobnostima i razvojnim karakteristikama.
                            Verujemo da je svako dete potpuno posebno i neponovljivo biće čije su potrebe jedinstvene, zato mu pružamo individualno usmeren i ličan pristup.
                            Želimo da svako dete živi i raste sa uverenjem da je baš takvo kakvo je, dovoljno dobro i dovoljno vredno da od nas odraslih nasledi "univerzum".
                            Društvo istraživača i pustolova, Univerzum nije samo mesto gde se odrasta, uči i sprema za neki budući život. Ovde se živi detinjstvo i stiču znanja i sposobnosti koja su deci korisna i vredna kako danas, tako i za sto godina.
                            </span>
                        </p>

                        <label for="post-1" class="read-more-trigger"></label>
                    </div>
                </div>
            </div>
            <div class="row">
                {{--<div class="col-lg-3 col-sm-6">--}}
                    {{--<div class="circle-responsive roze">--}}
                        {{--<div class="circle-content">Ponuda</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-3 col-sm-6">--}}
                    {{--<div class="circle-responsive plava">--}}
                        {{--<div class="circle-content">Program</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-3 col-sm-6">--}}
                    {{--<div class="circle-responsive ljubicasta">--}}
                        {{--<div class="circle-content">Dan u vrticu</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-3 col-sm-6">--}}
                    {{--<div class="circle-responsive zelena">--}}
                        {{--<div class="circle-content">Prostor</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                    <ul class="ch-grid">
                        <li>
                            <div class="ch-item ch-item1 ch-img-1">
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-1"></div>
                                        <div href="#PONUDA" class="ch-info-back ch-info-back1" data-toggle="modal">
                                            <h3>Ponuda</h3>
                                            <p>KLIKNI ZA DETALJE</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ch-item ch-item2 ch-img-2">
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-2"></div>
                                        <div href="#PROGRAM" class="ch-info-back ch-info-back2" data-toggle="modal">
                                            <h3>Program</h3>
                                            <p>KLIKNI ZA DETALJE</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ch-item ch-item3 ch-img-3">
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-3"></div>
                                        <div href="#DAN" class="ch-info-back ch-info-back3" data-toggle="modal">
                                            <h3>Dan u vrticu</h3>
                                            <p>KLIKNI ZA DETALJE</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="ch-item ch-item4 ch-img-4">
                                <div class="ch-info-wrap">
                                    <div class="ch-info">
                                        <div class="ch-info-front ch-img-4"></div>
                                        <div href="#PROSTOR" class="ch-info-back ch-info-back4" data-toggle="modal">
                                            <h3>Prostor</h3>
                                            <p>KLIKNI ZA DETALJE</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

            </div>
        </div>
    </section>




    <!-- Mailing list Section -->
    {{--<section id="mailing-list" class="bg-light-gray" >--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12 text-center">--}}
                    {{--<h2 class="section-heading">Mailing Lista</h2>--}}
                    {{--<h3 class="section-subheading text-muted">Unesite svoj mail kako bi dobijali novosti i obaveštenja od nas.</h3>--}}
                    {{--<form name="mailing-list" id="mailing-list-form" method="post" action="{{ route('add-mail') }}" >--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-6 col-md-offset-2">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="email" class="form-control" placeholder="Unesi svoju email adresu" id="email" name="email" required data-validation-required-message="Niste uneli ispravnu email adresu.">--}}
                                    {{--<p class="ml-error help-block text-danger"></p>--}}
                                    {{--<div class="ml-success"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2">--}}
                                {{--<div id="success"></div>--}}
                                {{--<button type="submit" id="submit-mailing-list" class="btn btn-xl prijavi-se">Prijavi se!</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}



    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="bg-light-gray">
        <div class="container zoomViewport">
            <div class="zoomContainer">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">Galerija</h2>
                        <h3 class="section-subheading text-muted">Pogledajte na slikama šta vas čeka u vrtiću Univerzum</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data- data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/001.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Centralni Prostor</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/002.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Predškolsko</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data- data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/003.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Naučni Kabinet</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/004.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Muzički Kabinet</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data- data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/005.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Ljubičasta učionica</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/006.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Garderoba</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/007.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Garderoba</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/008.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Garderoba</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/015.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Garderoba</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/014.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Garderoba</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/013.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Garderoba</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link zooms"  data-duration="500" data-targetsize="0.7" data-closeclick="true" data-preservescroll="true">
                            <img src="{{ asset('assets/photos/slike/012.jpg') }}" class="img-responsive" alt="">
                        </a>
                        <div class="portfolio-caption">
                            <h4>Garderoba</h4>
                            {{--<p class="text-muted">Ovde radimo to i to.</p>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    {{--<!-- About Section -->--}}
    {{--<section id="about">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12 text-center">--}}
                    {{--<h2 class="section-heading">Novosti</h2>--}}
                    {{--<h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}
                    {{--<ul class="timeline">--}}
                        {{--<li>--}}
                            {{--<div class="timeline-image">--}}
                                {{--<img class="img-circle img-responsive" src="../../../../../../assets/sites/default/img/about/1.jpg" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="timeline-panel">--}}
                                {{--<div class="timeline-heading">--}}
                                    {{--<h4>12.05.2015</h4>--}}
                                    {{--<h4 class="subheading">Nove usluge</h4>--}}
                                {{--</div>--}}
                                {{--<div class="timeline-body">--}}
                                    {{--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="timeline-inverted">--}}
                            {{--<div class="timeline-image">--}}
                                {{--<img class="img-circle img-responsive" src="../../../../../../assets/sites/default/img/about/2.jpg" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="timeline-panel">--}}
                                {{--<div class="timeline-heading">--}}
                                    {{--<h4>March 2011</h4>--}}
                                    {{--<h4 class="subheading">An Agency is Born</h4>--}}
                                {{--</div>--}}
                                {{--<div class="timeline-body">--}}
                                    {{--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<div class="timeline-image">--}}
                                {{--<img class="img-circle img-responsive" src="../../../../../../assets/sites/default/img/about/3.jpg" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="timeline-panel">--}}
                                {{--<div class="timeline-heading">--}}
                                    {{--<h4>December 2012</h4>--}}
                                    {{--<h4 class="subheading">Transition to Full Service</h4>--}}
                                {{--</div>--}}
                                {{--<div class="timeline-body">--}}
                                    {{--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="timeline-inverted">--}}
                            {{--<div class="timeline-image">--}}
                                {{--<img class="img-circle img-responsive" src="../../../../../../assets/sites/default/img/about/4.jpg" alt="">--}}
                            {{--</div>--}}
                            {{--<div class="timeline-panel">--}}
                                {{--<div class="timeline-heading">--}}
                                    {{--<h4>July 2014</h4>--}}
                                    {{--<h4 class="subheading">Phase Two Expansion</h4>--}}
                                {{--</div>--}}
                                {{--<div class="timeline-body">--}}
                                    {{--<p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="timeline-inverted">--}}
                            {{--<div class="timeline-image">--}}
                                {{--<h4>Be Part--}}
                                    {{--<br>Of Our--}}
                                    {{--<br>Story!</h4>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}

    <!-- Team Section -->
    <section id="team" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Ljudi</h2>
                    <h3 class="section-subheading text-muted">
                        Univerzum je jedinstveno mesto gde se principi koji se gaje u radu sa decom primenjuju i uvažavaju i na nivou našeg tima, od osnivača, preko direktora i vaspitača, roditelja, do kuvara, saradnika i dobavljača. To nas sve čini jednom velikom autentičnom zdravom i skladnom sredinom u kojoj se živi ono što se i uči (a najbolje se uči ono što se živi!).
                        Naši vaspitači i tim koji brine o deci su otvoreni, osećajni, prisutni i stručni i spremni da se svakoga dana usavršavaju i prilagođavaju. Tako napredujemo i učimo zajedno sa decom zbog koje smo ovde.

                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="team-member">
                        <img href="#TIJANAD" src="{{ asset('assets/photos/ljudi/tijanad.jpg') }}" class="img-responsive img-circle ljudi" alt="" data-toggle="modal">
                        <h4>Tijana Dapcevic</h4>
                        <p class="text-muted">Profesor Muzike</p>
                        {{--<ul class="list-inline social-buttons">--}}
                            {{--<li><a href="#"><i class="fa fa-twitter"></i></a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#"><i class="fa fa-facebook"></i></a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#"><i class="fa fa-linkedin"></i></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img href="#TIJANAD" src="{{ asset('assets/photos/ljudi/tijanad.jpg') }}" class="img-responsive img-circle ljudi" alt="" data-toggle="modal">
                        <h4>Tijana Dapcevic</h4>
                        <p class="text-muted">Profesor Muzike</p>
                        {{--<ul class="list-inline social-buttons">--}}
                        {{--<li><a href="#"><i class="fa fa-twitter"></i></a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#"><i class="fa fa-facebook"></i></a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#"><i class="fa fa-linkedin"></i></a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img href="#TIJANAD" src="{{ asset('assets/photos/ljudi/tijanad.jpg') }}" class="img-responsive img-circle ljudi" alt="" data-toggle="modal">
                        <h4>Tijana Dapcevic</h4>
                        <p class="text-muted">Profesor Muzike</p>
                        {{--<ul class="list-inline social-buttons">--}}
                        {{--<li><a href="#"><i class="fa fa-twitter"></i></a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#"><i class="fa fa-facebook"></i></a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#"><i class="fa fa-linkedin"></i></a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                    </div>
                </div>


            </div>
        </div>
    </section>


    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Kontaktiraj Nas</h2>
                    <h3 class="section-subheading " style="color:white;">Društvo istraživača i pustolova, Univerzum je otvoreno smo za sva vaša pitanjaUkoliko želite da se vidimo i upoznamo, pozovite nas ili zakažite razgovor i obilazak vrtića putem formulara ispod.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Ime *" id="name" required data-validation-required-message="Unesi ime.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email *" id="contact-email" required data-validation-required-message="Unesi ispravan email.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Vaš broj telefona *" id="phone" required data-validation-required-message="Unesi broj telefona.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Poruka *" id="message" required data-validation-required-message="Unesi tekst poruke."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <div id="success"></div>
                                    <button type="submit" class="btn btn-xl">Pošalji Poruku</button>
                                </div>
                            </div>

                            <div class="col-md-6 col-md-offset-2">
                                <div class="row" id="gmap" style="width:100%; height:250px;"></div>
                                <div class="row kontakt-podaci" >
                                    <h4>Vrtić Univerzum</h4>
                                    Bulevar Mihaila Pupina 165a<br>
                                    11000 Beograd, Srbija<br>
                                    Telefon: +381 (064) 995-3870<br>
                                    Email: info@vrticuniverzum.rs
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Univerzum</span>
                </div>
                {{--<div class="col-md-4">--}}
                    {{--<ul class="list-inline social-buttons">--}}
                        {{--<li><a href="#"><i class="fa fa-twitter"></i></a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#"><i class="fa fa-facebook"></i></a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#"><i class="fa fa-linkedin"></i></a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="col-md-4">--}}
                    {{--<ul class="list-inline quicklinks">--}}
                        {{--<li><a href="#">Privatnost</a>--}}
                        {{--</li>--}}
                        {{--<li><a href="#">Pravila Koriscenja</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </div>
        </div>
    </footer>

    {{--<!-- Portfolio Modals -->--}}
    {{--<!-- Use the modals below to showcase details about your portfolio projects! -->--}}

    <!--PONUDA MODAL -->
    <div class="portfolio-modal modal fade" id="PONUDA" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>PONUDA</h2>
                            <div>
                                {{ $ponuda }}
                            </div>
                            <br>
                            <button type="submit" class="btn btn-xl" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--PROGRAM MODAL -->
    <div class="portfolio-modal modal fade" id="PROGRAM" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>PROGRAM</h2>
                            <div>
                                {{ $program }}
                            </div>
                            <br>
                            <button type="submit" class="btn btn-xl" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--DAN U VRTICU MODAL -->
    <div class="portfolio-modal modal fade" id="DAN" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>DAN U VRTICU</h2>
                            <div>
                                {{ $dan }}
                            </div>
                            <br>
                            <button type="submit" class="btn btn-xl" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--PROSTOR MODAL -->
    <div class="portfolio-modal modal fade" id="PROSTOR" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>PROSTOR</h2>
                            <div>
                               {{ $prostor }}
                            </div>
                            <br>
                            <button type="submit" class="btn btn-xl" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="portfolio-modal modal fade" id="TIJANAD" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>TIJANA DAPCEVIC</h2>
                            <div>
                                <img src="{{ asset('assets/photos/ljudi/tijanad.jpg') }}" class="img-responsive img-circle ljudi-slika" alt="">
                            </div>
                            <div>
                                Tijana je rođena 3. februara 1976. godine u Skoplju.
                                U svojoj petoj godini upisala je nižu muzičku školu, a u sedmoj počela da svira violončelo. Tokom školovanja, učestvovala je na nekoliko republičkih i saveznih takmičenja (tadašnje) Jugoslavije i osvojila značajne nagrade. Svirala je u Makedonskoj filharmoniji, a takođe i u jednom od najpopularnijih orkestara u Lajpcigu.
                                Diplomirala je 1999. godine na Muzičkooj akademiji u Skoplju, odsek: violončelo.
                                Bila je voditelj na radiju, televiziji i glumila u pozorištu.
                                Sredinom 2000. godine, počinje da živi i radi u Beogradu gde, godinu dana kasnije, izdaje svoj prvi album pod nazivom Kao da.. za izdvačku kuću City Records.
                                Tokom godina, Tijana je bila učesnik mnogobrojnih festivala, kao što su: Evrovizija (Danska, ’14.), Sunčane skale (1. mesto, ’02.) , Pjesma mediterana (1. mesto, ’05.), Radijski festival (1. mesto ’06.) itd.
                                Dobitnik je mnogih godišnjih nagrada i priznanja, iza sebe trenutno ima pet albuma, a uspela je da ostvari i svoju dugogodišnju želju i oproba se kao glumica, dobivši uloge u nekoliko predstava, koje su trenutno na redovnim repertoarima Beogradskih pozorišta.
                                Jedina neostvarena želja joj je da dobije ulogu u nekom filmu.
                            </div>
                            <br>
                            <button type="submit" class="btn btn-xl" data-dismiss="modal"><i class="fa fa-times"></i> Zatvori </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop