
@section('title')
    Uloguj se -
    @parent
@stop

<link rel="stylesheet" href="{{ asset('assets/bootstrap/css/cosmo.min.css') }}">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

@section ('footer-scripts')

    <script src="{{ asset('assets/js/jquery-2.1.3.min.js') }}"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
@show


@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            {{-- Notifications --}}
            @include('layouts.notifications')
        </div>
    </div>
    <div class="col-lg-6 col-lg-offset-3 register-form">
        <div class="well bs-component">
            <form id="login" action="{{ route('login') }}" method="post" class="form-horizontal" autocomplete="off">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <legend>Uloguj se</legend>
                <div class="form-group">
                    <label for="pimName" class="col-lg-3 control-label">Korisničko ime:</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Unesi naziv firme" autocomplete="off" value="{{ Input::old('username') }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pimPassword" class="col-lg-3 control-label">Šifra</label>
                    <div class="col-lg-9">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Izaberi sifru" autocomplete="off" minlength="5" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12" style="text-align:center;">
                        <input type="submit" data-order="1" value="Uloguj me" class="btn btn-primary next-button"/>
                    </div>
                </div>
            </form>
        </div>

    </div>
@show