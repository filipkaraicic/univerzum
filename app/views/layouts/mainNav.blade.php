<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{{ $site->username }}</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                <li class="{{ Request::is( '/') ? 'active' : '' }}"><a href="{{ route('home') }}">Home </a></li>
                <li class="{{ Request::is( 'onama') ? 'active' : '' }}"><a href="{{ route('register') }}">O nama</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="fancy_border">Uloguj se</a></li>
            </ul>
        </div>
    </div>
</nav>