{{--@if ($errors->any())
    <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
        <h4>Error</h4>
        @foreach($errors->getMessages() as $error)
        <p>{{ $error[0] }}</p>
        @endforeach
    </div>
@endif--}}

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
	<h4>Uspeh!</h4>
	<p>{{ $message }}</p>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
	<h4>Greska</h4>
	<p>{{ $message }}</p>
    <ul>
        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
	<h4>Upozorenje</h4>
	<p>{{ $message }}</p>
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
	<h4>Info</h4>
	<p>{{ $message }}</p>
</div>
@endif