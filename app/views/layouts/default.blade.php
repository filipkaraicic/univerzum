<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        @section('title')
            Univerzum
        @show
    </title>
    <meta name="description" content="Vrtic Univerzum"/>
    {{-- Mobile Specific Metas
    ================================================== --}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {{-- CSS
    ================================================== --}}
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/cosmo.min.css') }}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/sites/default/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/timecircles/TimeCircles.css') }}">


    <link rel="stylesheet" type="text/css" href="{{ asset('assets/sliderengine/amazingslider-1.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/circles/common.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/circles/style6.css') }}">

    @section('css')

    @show

    {{-- JS
    ================================================== --}}
    @section('header-scripts')

        <script src="{{ asset('assets/js/jquery-2.1.3.min.js') }}"></script>
        <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>

        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="{{ asset('assets/sites/default/js/cbpAnimatedHeader.js') }}"></script>

        <!-- Contact Form JavaScript -->
        <script src="{{ asset('assets/sites/default/js/jqBootstrapValidation.js') }}"></script>
        <script src="{{ asset('assets/sites/default/js/contact_me.js') }}"></script>

        <!-- Custom Theme JavaScript -->
        <script src="{{ asset('assets/sites/default/js/agency.js') }}"></script>
        <script src="{{ asset('assets/js/main.js') }}"></script>
        <script src="{{ asset('assets/timecircles/TimeCircles.js') }}"></script>


        <script src="{{ asset('assets/sliderengine/amazingslider.js') }}"></script>
        <script src="{{ asset('assets/sliderengine/initslider-1.js') }}"></script>

        <script src="{{ asset('assets/js/jquery.zoomooz.min.js') }}"></script>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=true"></script>
        <script src="{{ asset('assets/js/gmaps.js') }}"></script>


    @show

    {{--TEMPLATE SPECIFIC--}}
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    {{--Google apps verification--}}
    <meta name="google-site-verification" content="yj34xndgqs2DR1rCI5-4YhEQTflbpC612OTwMNi2YY4" />
</head>

<body id="page-top" class="index">

@yield('content')

{{-- Footer Scripts --}}
@section('footer-scripts')
    {{ $js_base_url }}
@show

{{-- Popups --}}
@section('modals')

@show

</body>
</html>