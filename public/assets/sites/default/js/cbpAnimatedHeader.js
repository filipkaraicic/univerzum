/**
 * cbpAnimatedHeader.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */


$(function(){
    var docElem = document.documentElement,
        header = $( '.navbar-default' ),
        didScroll = false,
        changeHeaderOn = 300;

    function init() {
        window.addEventListener( 'scroll', function( event ) {
            if( !didScroll ) {
                didScroll = true;
                setTimeout( scrollPage, 100 );
            }
        }, false );
    }

    function scrollPage() {
        var sy = scrollY();
        if ( sy >= changeHeaderOn ) {
            header.addClass('navbar-shrink');
            $("#site_name").removeClass("fadeout").removeClass("show_title");
            $("#site_name").removeClass("fadein").addClass("hide_title");
        }
        else {
            header.removeClass('navbar-shrink');
            $("#site_name").removeClass("fadeout").removeClass("hide_title");
            $("#site_name").removeClass("fadeout").addClass("show_title");
        }
        didScroll = false;
    }

    function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
    }

    function animate_name(){
        if ($("#site_name").hasClass("fadeout"))
            $("#site_name").removeClass("fadeout").addClass("fadein");
        else
            $("#site_name").removeClass("fadein").addClass("fadeout");
    }

    init();
    scrollPage();
})





