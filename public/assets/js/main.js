$(function(){

    $("#DateCountdown").TimeCircles({
        "animation": "smooth",
        "bg_width": 1.2,
        "fg_width": 0.1,
        "circle_bg_color": "#60686F",
        "time": {
            "Days": {
                "text": "DANA",
                "color": "#D555A8",
                "show": true
            },
            "Hours": {
                "text": "SATI",
                "color": "#2DCBD4",
                "show": true
            },
            "Minutes": {
                "text": "MINUTA",
                "color": "#9E5ABD",
                "show": true
            },
            "Seconds": {
                "text": "SEKUNDI",
                "color": "#C7D05B",
                "show": true
            }
        }
    });

    remove_logo = setInterval(function(){
        $('[href="http://amazingslider.com"]').parent().css('left','-5000px');
    },100)

    setTimeout(function(){ clearInterval(remove_logo)}, 5000);

    $("#mailing-list-form").submit(function(e){
        e.preventDefault();
        addMail();

    });

    current_width = $(window).width();
    if (current_width > 768) {
            //$(".zooms").zoomTarget();
        $(".zooms").addClass('zoomTarget');
    }


    map = new GMaps({
        div: '#gmap',
        lat: 44.822277,
        lng: 20.415344
    });
   
    map.addMarker({
        lat: 44.822277,
        lng: 20.415344,
        title: 'Univerzum',
        infoWindow: {
            content: '<p>Univerzum</p>'
        },
        click: function(e) {

        }
    });



    //READ MORE ZA O NAMA
    var $el, $ps, $up, totalHeight;

    $(".sidebar-box .button").click(function() {

        totalHeight = 0

        $el = $(this);
        $p  = $el.parent();
        $up = $p.parent();
        $ps = $up.find("p:not('.read-more')");

        // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
        $ps.each(function() {
            totalHeight += $(this).outerHeight();
        });

        $up
            .css({
                // Set height to prevent instant jumpdown when max height is removed
                "height": $up.height(),
                "max-height": 9999
            })
            .animate({
                "height": totalHeight
            });

        // fade out read-more
        $p.fadeOut();

        // prevent jump-down
        return false;

    });


})


function addMail() {
    form_data = $("#mailing-list-form").serialize();
    $.ajax({
        type:     'POST',
        dataType: 'json',
        url:      $("#mailing-list-form").attr("action"),
        data:     form_data,
        success:  function (data) {
            if (data === "success") {
                $(".ml-error").hide();
                $(".ml-success").html('Hvala na prijavi!');
                $("#email").attr('disabled', 'disabled');
                $("#submit-mailing-list").attr('disabled', 'disabled');
            } else {
                $(".ml-error").html('Email nije ispravan ili ste vec prijavljeni na listu.');
            }
        },
        error:    function (xhr, ajaxOptions, thrownError) {
            $(".ml-error").html('Zao nam je doslo je do greske. Proverite da li je mail ispravan.');
            console.log('ERROR IN MAILING LIST');
            console.log(xhr.responseText);
            console.log(thrownError);
        }
    });

}