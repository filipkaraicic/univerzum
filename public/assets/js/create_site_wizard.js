initialized_maps = false;

$(function() {

    form = $("#pimRegister");

    // ------KRETANJE KROZ KORAKE
    $('.next-button').click(function (e) {
        e.preventDefault();
        current = $(this).data("order");
        next = current + 1;

        if ( current != 4 ) {
            validateForm();
            if (!form.valid()) {
                return;
            } else if (current == 3) {
                tab = $('.nav-tabs .active').text();
                if (tab != 'Nedelja') {
                    validateForm();
                    if (!form.valid()) {
                        return false;
                    }
                    $('.nav-tabs .active')
                        .next()
                        .find('a[data-toggle="tab"]')
                        .click();
                    return;
                }

            }
        }


        $(".wizard_step_" + current).hide();
        $(".wizard_step_" + next).show();
        if (!initialized_maps) {
            initialized_maps = true;
            initialize();
            // initializeSearch();
        }

        done = (current / 4) * 100;
        $('.wizard_current_progress').width(done + "%");
    })

    $('.prev-button').click(function (e) {
        e.preventDefault();
        current = $(this).data("order");
        prev = current - 1;

        if (current == 3) {
            tab = $('.nav-tabs .active').text();
            if (tab != 'Pon-Pet') {
                $('.nav-tabs .active')
                    .prev()
                    .find('a[data-toggle="tab"]')
                    .click();
                return;
            }

        }

        $(".wizard_step_" + current).hide();
        $(".wizard_step_" + prev).show();

        if (prev == 1) {
            $('.wizard_current_progress').width("0%");
        } else {
            done = ((prev - 1) / 4) * 100;
            $('.wizard_current_progress').width(done + "%");
        }

    })
    // ------KRETANJE KROZ KORAKE KRAJ

    //---------------------------------------------VREME
    $("#nedelja_non-stop").change(function () {
        if (this.checked) {
            $("#nedelja_ne_radi").prop('checked', false);
            $("#nedelja select").prop('disabled', true);
            $(this).prop('disabled', false);
            $("#nedelja select").val("");
        } else {
            $("#nedelja input").prop('disabled', false);
            $("#nedelja select").prop('disabled', false);
            $(this).prop('disabled', false);
        }
    })

    $("#nedelja_ne_radi").change(function () {
        if (this.checked) {
            $("#nedelja_non-stop").prop('checked', false);
            $("#nedelja select").prop('disabled', true);
            $(this).prop('disabled', false);
            $("#nedelja select").val("");
        } else {
            $("#nedelja input").prop('disabled', false);
            $("#nedelja select").prop('disabled', false);
            $(this).prop('disabled', false);
        }
    })

    $("#radni_non-stop").change(function () {
        if (this.checked) {
            $("#radni_ne_radi").prop('checked', false);
            $("#radni select").prop('disabled', true);
            $(this).prop('disabled', false);
            $("#radni select").val("");
        } else {
            $("#radni select").prop('disabled', false);
            $(this).prop('disabled', false);
        }
    })

    $("#radni_ne_radi").change(function () {
        if (this.checked) {
            $("#radni_non-stop").prop('checked', false);
            $("#radni select").prop('disabled', true);
            $(this).prop('disabled', false);
            $("#radni select").val("");
        } else {
            $("#radni select").prop('disabled', false);
            $(this).prop('disabled', false);
        }
    })

    $("#subota_non-stop").change(function () {
        if (this.checked) {
            $("#subota_ne_radi").prop('checked', false);
            $("#subota select").prop('disabled', true);
            $(this).prop('disabled', false);
            $("#subota select").val("");
        } else {
            $("#subota input").prop('disabled', false);
            $("#subota select").prop('disabled', false);
            $(this).prop('disabled', false);
        }
    })

    $("#subota_ne_radi").change(function () {
        if (this.checked) {
            $("#subota_non-stop").prop('checked', false);
            $("#subota select").prop('disabled', true);
            $(this).prop('disabled', false);
            $("#subota select").val("");
        } else {
            $("#subota input").prop('disabled', false);
            $("#subota select").prop('disabled', false);
            $(this).prop('disabled', false);
        }
    })
    //----------------------------------------------VREME KRAJ

    //UPLOAD PHOTOS
    var cropperOptions1 = {
        uploadUrl: base_url + '/upload-photo',
        cropUrl: base_url + '/crop-photo',
        customUploadButtonId: 'upload_profile_photo1',
        outputUrlId: 'profile_photo1_url'
    }

    var cropperOptions2 = {
        uploadUrl: base_url + '/upload-photo',
        cropUrl: base_url + '/crop-photo',
        customUploadButtonId: 'upload_profile_photo2',
        outputUrlId: 'profile_photo2_url'
    }

    var cropperOptions3 = {
        uploadUrl: base_url + '/upload-photo',
        cropUrl: base_url + '/crop-photo',
        customUploadButtonId: 'upload_profile_photo3',
        outputUrlId: 'profile_photo3_url'
    }

    var cropperHeader1 = new Croppic('profile_photo1', cropperOptions1);
    var cropperHeader2 = new Croppic('profile_photo2', cropperOptions2);
    var cropperHeader3 = new Croppic('profile_photo3', cropperOptions3);

    //UPLOAD PHOTOS END

    //FORM VALIDATION

    function validateForm() {
        form.validate({
            onkeyup: false,
            rules: {
                username: {
                    required: true,
                    remote: {
                        url: base_url + "/check-username",
                        async:false,
                        type: "post"
                    }
                },
                email: {
                    required: true,
                    remote: {
                        url: base_url + "/check-email",
                        async:false,
                        type: "post"
                    }
                }
            },
            messages: {
                username: {
                    required: "Unesi naziv firme.",
                    remote: "Firma vec postoji, izaberite drugo ime."
                },
                pimPassword: {
                    required: "Izaberite vas password.",
                    minlength: "Password mora imati bar 5 karaktera."

                },
                email: {
                    remote: "Korisnik sa tim mailom vec postoji."
                }

            }

        });
    }



    jQuery.extend(jQuery.validator.messages, {
        required: "Polje je obavezno."
    });

    //POSEBNA PRAVILA ZA VALIDACIJU VREMENA

    // add the rule here
    jQuery.validator.addMethod("izabranovreme", function (value, element) {

        element_name = $(element).attr('id');
        switch (element_name) {
            case 'radni_od':
            case 'radni_do':
                nonstop = $('#radni_non-stop').is(':checked');
                neradi = $('#radni_ne_radi').is(':checked');
                break;
            case 'subota_od':
            case 'subota_do':
                nonstop = $('#subota_non-stop').is(':checked');
                neradi = $('#subota_ne_radi').is(':checked');
                break;
            case 'nedelja_od':
            case 'nedelja_do':
                nonstop = $('#nedelja_non-stop').is(':checked');
                neradi = $('#nedelja_ne_radi').is(':checked');
                break;
        }
        if (!(nonstop || neradi)) {
            if (value == "")
                return false;
        }

        return true;
    }, "Izaberi vreme.");


    //VALIDATE TABS ON TIME PANEL ON SWITCH
    $(".nav-tabs li a").click(function(e){
        if(e.hasOwnProperty('originalEvent'))
            return false;
    })





});